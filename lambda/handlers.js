// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const util = require('./util');
const logic = require('./logic');

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    async handle(handlerInput) {
        const {attributesManager} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        
        let voiceData = '';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                if (resultPermission.permissions.length > 0) {
                    return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
                }
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        const name = sessionAttributes['name'] ? sessionAttributes['name'] + '. ' : '';

        if (!sessionAttributes['deviceId']) {
            voiceData = handlerInput.t('WELCOME_UNREGISTERED_DEVICE_MSG', {name: name});
            if (util.supportsAPL(handlerInput)) {
                voiceData += handlerInput.t('MISSING_DEVICE_NOT_BE_REGISTERED');
            } else {
                handlerInput.responseBuilder.addDelegateDirective({
                    name: 'RegisterDeviceIntent',
                    confirmationStatus: 'NONE',
                    slots: {}
                })
            }
        } else {
            voiceData = handlerInput.t('WELCOME_MSG', {name: name});
        }        
        logic.getLaunchScreen(handlerInput, voiceData);
        return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
    }
};
const RegisterDeviceIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'RegisterDeviceIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;

        let voiceData = '';
        
        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        const name = sessionAttributes['name'] ? sessionAttributes['name'] : '';
        const email = sessionAttributes['email'] ? sessionAttributes['email'] : '';

        //Inicia registro de dispositivo
        if (!sessionAttributes['deviceId']) {
            try {
                try { // call the progressive response service
                    await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_REGISTER_DEVICE_MSG'));
                } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                    console.log("Progressive directive error : " + error);
                }
                
                let response = await logic.httpPost('RegisterDevice', {"deviceId": logic.encodeBase64(deviceId), "email": logic.encodeBase64(email)});
                if (response.success) {
                    sessionAttributes['deviceId'] = deviceId;
                }
                voiceData = handlerInput.t(`${response.code}_MSG`) + handlerInput.t('SHORT_HELP_MSG');
                
                logic.getLaunchScreen(handlerInput, voiceData);
                
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } catch (error) {
                console.log(error);
                //El registro del dispositivo fallo, se selecciona mensaje dependiendo de la respuesta
                logic.getLaunchScreen(handlerInput, handlerInput.t('DEVICE_NOT_BE_REGISTERED'));
                return handlerInput.responseBuilder.speak(handlerInput.t('DEVICE_NOT_BE_REGISTERED') + handlerInput.t('ERROR_CONTACT_TO_ILSP')).getResponse();
            }
        } else {
            logic.getLaunchScreen(handlerInput, handlerInput.t('DEVICE_ALREADY_REGISTERED'));
            return handlerInput.responseBuilder.speak(handlerInput.t('DEVICE_ALREADY_REGISTERED')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const StatusCurrentServicesIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'StatusCurrentServicesIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';
        
        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {
            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (parseInt(model.totalServices) > 0) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        StatusCurrentServicesIntent_DATA_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_TOTAL_MSG`, {
                            count: parseInt(model.totalServices)
                        }),
                        StatusCurrentServicesIntent_DATA_TYPES_CONJUNCTION_RESULT: (function () {
                            if (parseInt(model.totalForeignServices) > 0 && parseInt(model.totalLocalServices) > 0) {
                                return handlerInput.t(`${intent.name}_DATA_TYPES_CONJUNCTION_MSG`, {
                                    StatusCurrentServicesIntent_DATA_FOREIGN_RESULT: handlerInput.t(`${intent.name}_DATA_FOREIGN_MSG`, {
                                        count: parseInt(model.totalForeignServices)
                                    }),
                                    StatusCurrentServicesIntent_DATA_LOCALS_RESULT: handlerInput.t(`${intent.name}_DATA_LOCALS_MSG`, {
                                        count: parseInt(model.totalLocalServices)
                                    })
                                });
                            } else if (parseInt(model.totalForeignServices) > 0 && parseInt(model.totalLocalServices) === 0) {
                                return handlerInput.t(`${intent.name}_DATA_ALL_FOREIGN_MSG`);
                            } else if (parseInt(model.totalForeignServices) === 0 && parseInt(model.totalLocalServices) > 0) {
                                return handlerInput.t(`${intent.name}_DATA_ALL_LOCALS_MSG`);
                            }
                            return '';
                        })(),  
                        StatusCurrentServicesIntent_DATA_RISK_ARRIVING_RESULT: (function () {
                            if (parseInt(model.totalServicesRiskArrivingLate) > 0) {
                                return handlerInput.t(`${intent.name}_DATA_RISK_ARRIVING_MSG`, {
                                    StatusCurrentServicesIntent_DATA_RISK_ARRIVING_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_RISK_ARRIVING_TOTAL_MSG`, {
                                        count: parseInt(model.totalServicesRiskArrivingLate)
                                    }),
                                    totalPercentageServicesRiskArrivingLate: parseInt(model.totalPercentageServicesRiskArrivingLate)
                                });
                            } else {
                                return handlerInput.t(`${intent.name}_DATA_NO_RISK_ARRIVING_MSG`);
                            }
                        })()
                    });
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                if (model.destinations.length > 0) {
                    var items = model.destinations.map((item, index) => {
                        return {
                            listItemIdentifier: item.destinyName,
                            ordinalNumber: index + 1,
                            textContent: {
                                parameterTitle: {
                                    type: 'PlainText',
                                    text: item.destinyName
                                },
                                parameterSubtitle: {
                                    type: 'RichText',
                                    text: item.state
                                },
                                total: {
                                    type: 'PlainText',
                                    text: item.total
                                }
                            },
                            token: item.destinyName
                        };
                    });
                    logic.getVerticalGridScreen(handlerInput, items);
                } else {
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            logic.getSimpleScreen(handlerInput, handlerInput.t('NOT_DATA_FOUND_MSG'));
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const SummaryByTransportLineIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'SummaryByTransportLineIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {
            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (model.totalTransportLines) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        SummaryByTransportLineIntent_DATA_TOTAL_LT_RESULT: handlerInput.t(`${intent.name}_DATA_TOTAL_LT_RESULT`, {
                            count: parseInt(model.totalTransportLines)
                        }),
                        transportLineName: model.transportLineName,
                        SummaryByTransportLineIntent_DATA_TOTAL_TS_RESULT: handlerInput.t(`${intent.name}_DATA_TOTAL_TS_MSG`, {
                            count: parseInt(model.totalServices)
                        })
                    });
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                logic.getSimpleScreen(handlerInput, voiceData);
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            logic.getSimpleScreen(handlerInput, handlerInput.t('NOT_DATA_FOUND_MSG'));
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const DetailsByShipmentNumberIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'DetailsByShipmentNumberIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let textData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {
            slotValue = intent.slots.shipmentNumber.value;            

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                if (response.items !== null) {
                    const model = response.items;
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        shipmentNumber: model.shipmentNumber, 
                        transportLineName: model.transportLineName, 
                        economic: model.economic,
                        operatorName: model.operatorName,
                        operatorPhone: model.operatorPhone,
                        address: `${model.municipality}${model.municipality.length > 0 ? ', ': ''}${model.state}`
                    });
                    var data = [
                        model.transportLineName, 
                        model.economic,
                        model.operatorName,
                        model.operatorPhone,
                        `${model.municipality}${model.municipality.length > 0 ? ', ': ''}${model.state}`
                    ];
                    var dataContent = {
                        title: handlerInput.t('THE_SHIPMENT_NUMBER', {shipmentNumber: model.shipmentNumber}),
                        content: data.filter(item => item.length > 0).map(item => `• ${item}`).join('<br>')
                    };
                    logic.getCardDetailScreen(handlerInput, dataContent);
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            logic.getSimpleScreen(handlerInput, handlerInput.t('NOT_DATA_FOUND_MSG'));
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const DepartureStatusLastQuarterIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'DepartureStatusLastQuarterIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (parseInt(model.totalServices) > 0) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        DepartureStatusLastQuarterIntent_DATA_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_TOTAL_MSG`, {
                            count: parseInt(model.totalServices)
                        }),
                        DepartureStatusLastQuarterIntent_DATA_TL_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_TL_TOTAL_MSG`, {
                            count: parseInt(model.totalTransportLines)
                        }),
                        DepartureStatusLastQuarterIntent_DATA_ONTIME_RESULT: (function () {
                            if (parseInt(model.totalServicesOnTime) > 0) {
                                return handlerInput.t(`${intent.name}_DATA_ONTIME_MSG`, {
                                    DepartureStatusLastQuarterIntent_DATA_ONTIME_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_ONTIME_TOTAL_MSG`, {
                                        count: parseInt(model.totalServicesOnTime)
                                    }),
                                    percentageTotalServicesOnTime: parseInt(model.percentageTotalServicesOnTime)
                                });
                            } else {
                                return handlerInput.t(`${intent.name}_DATA_NO_ONTIME_MSG`);
                            }
                        })(),
                        address: `${model.municipality}${model.municipality.length > 0 ? ', ': ''}${model.state}`
                    });
                    
                    var data = [];
                    if (model.destinies.length > 0)  {
                        data.push({
                            listItemIdentifier: handlerInput.t(`DEPARTURES_BY_CLIENT_VS_DESTINATION_MSG`),
                            textContent: {
                                secondaryText: {
                                    type: "PlainText",
                                    text: handlerInput.t(`DEPARTURES_BY_CLIENT_VS_DESTINATION_MSG`)
                                }
                            },
                            listTemplate1ListData: {
                                type: "list",
                                listId: "lt1Sample",
                                totalNumberOfItems: 10,
                                listPage: {
                                    listItems: model.destinies.map((item, index) => {
                                        return {
                                            listItemIdentifier: item.destinyName,
                                            ordinalNumber: index + 1,
                                            textContent: {
                                                parameterTitle: {
                                                    type: 'PlainText',
                                                    text: item.destinyName
                                                },
                                                parameterSubtitle: {
                                                    type: 'RichText',
                                                    text: item.state
                                                },
                                                total: {
                                                    type: 'PlainText',
                                                    text: item.total
                                                }
                                            },
                                            token: item.destinyName
                                        };
                                    })
                                }
                            },
                            token: handlerInput.t(`DEPARTURES_BY_CLIENT_VS_DESTINATION_MSG`)
                        });
                    }
                    
                    if (model.transportLines.length > 0)  {
                        data.push({
                            listItemIdentifier: handlerInput.t(`SALIDAS_POR_LINEA_TRANSPORTISTA_MSG`),
                            textContent: {
                                secondaryText: {
                                    type: "PlainText",
                                    text: handlerInput.t(`SALIDAS_POR_LINEA_TRANSPORTISTA_MSG`)
                                }
                            },
                            listTemplate1ListData: {
                                type: "list",
                                listId: "lt1Sample",
                                totalNumberOfItems: 10,
                                listPage: {
                                    listItems: model.transportLines.map((item, index) => {
                                        return {
                                            listItemIdentifier: item.transportLineName,
                                            ordinalNumber: index + 1,
                                            textContent: {
                                                parameterTitle: {
                                                    type: 'PlainText',
                                                    text: item.transportLineName
                                                },
                                                parameterSubtitle: {
                                                    type: 'RichText',
                                                    text: item.contactName
                                                },
                                                total: {
                                                    type: 'PlainText',
                                                    text: item.total
                                                }
                                            },
                                            token: item.transportLineName
                                        };
                                    })
                                }
                            },
                            token: handlerInput.t(`SALIDAS_POR_LINEA_TRANSPORTISTA_MSG`)
                        });
                    }
                    
                    if (data.length > 0) {
                        logic.getMultiVerticalGridScreen(handlerInput, data);
                    } else {
                        logic.getSimpleScreen(handlerInput, voiceData);
                    }
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const CurrentSecurityStatusIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'CurrentSecurityStatusIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (model.shipmentNumber !== null) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        shipmentNumber: model.shipmentNumber, 
                        riskPercentage: parseInt(model.riskPercentage),
                        CurrentSecurityStatusIntent_DATA_CONJUNCTION_RESULT: (function () {
                            if (parseInt(model.totalServicesOnReaction) > 0 && parseInt(model.totalServicesOnProcessTheft) > 0) {
                                return handlerInput.t(`${intent.name}_DATA_CONJUNCTION_MSG`, {
                                    CurrentSecurityStatusIntent_DATA_REACTION_RESULT: handlerInput.t(`${intent.name}_DATA_REACTION_MSG`, {
                                        count: parseInt(model.totalServicesOnReaction)
                                    }),
                                    CurrentSecurityStatusIntent_DATA_THEFT_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_MSG`, {
                                        count: parseInt(model.totalServicesOnProcessTheft)
                                    })
                                });
                            } else if (parseInt(model.totalServicesOnReaction) > 0 && parseInt(model.totalServicesOnProcessTheft) === 0) {
                                return handlerInput.t(`${intent.name}_DATA_CONJUNCTION_MSG`, {
                                    CurrentSecurityStatusIntent_DATA_REACTION_RESULT: handlerInput.t(`${intent.name}_DATA_REACTION_MSG`, {
                                        count: parseInt(model.totalServicesOnReaction)
                                    }),
                                    CurrentSecurityStatusIntent_DATA_THEFT_RESULT: handlerInput.t(`${intent.name}_DATA_NO_THEFT_MSG`)
                                });
                            } else if (parseInt(model.totalServicesOnReaction) === 0 && parseInt(model.totalServicesOnProcessTheft) > 0) {
                                return handlerInput.t(`${intent.name}_DATA_CONJUNCTION_MSG`, {
                                    CurrentSecurityStatusIntent_DATA_REACTION_RESULT: handlerInput.t(`${intent.name}_DATA_NO_REACTION_MSG`),
                                    CurrentSecurityStatusIntent_DATA_THEFT_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_MSG`, {
                                        count: parseInt(model.totalServicesOnProcessTheft)
                                    })
                                });
                            } else {
                                return handlerInput.t(`${intent.name}_DATA_NO_REACTION_NO_THEFT_MSG`);
                            }
                        })()
                    });
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                var data = [];
                if (model.servicesOnReaction.length > 0)  {
                    data.push({
                        listItemIdentifier: handlerInput.t(`SERVICES_IN_VALIDATION_BY_REACTION_MSG`),
                        textContent: {
                            secondaryText: {
                                type: "PlainText",
                                text: handlerInput.t(`SERVICES_IN_VALIDATION_BY_REACTION_MSG`)
                            }
                        },
                        listTemplate1ListData: {
                            type: "list",
                            listId: "lt1Sample",
                            totalNumberOfItems: 10,
                            listPage: {
                                listItems: model.servicesOnReaction.map((item, index) => {
                                    var dataDetail = [
                                        item.transportLineName, 
                                        item.economic,
                                        item.operatorName,
                                        item.operatorPhone,
                                        item.address
                                    ];
                                    return {
                                        listItemIdentifier: item.shipmentNumber,
                                        ordinalNumber: index + 1,
                                        textContent: {
                                            parameterTitle: {
                                                type: 'PlainText',
                                                text: item.shipmentNumber
                                            },
                                            parameterSubtitle: {
                                                type: 'RichText',
                                                text: dataDetail.filter(item => item.length > 0).map(item => `• ${item}`).join('<br>')
                                            },
                                            total: {
                                                type: 'PlainText',
                                                text: ''
                                            }
                                        },
                                        token: item.shipmentNumber
                                    };
                                })
                            }
                        },
                        token: handlerInput.t(`SERVICES_IN_VALIDATION_BY_REACTION_MSG`)
                    });
                }
                
                if (model.servicesOnProcessTheft.length > 0)  {
                    data.push({
                        listItemIdentifier: handlerInput.t(`SERVICES_IN_THE_PROCESS_OF_THEFT_MSG`),
                        textContent: {
                            secondaryText: {
                                type: "PlainText",
                                text: handlerInput.t(`SERVICES_IN_THE_PROCESS_OF_THEFT_MSG`)
                            }
                        },
                        listTemplate1ListData: {
                            type: "list",
                            listId: "lt1Sample",
                            totalNumberOfItems: 10,
                            listPage: {
                                listItems: model.servicesOnProcessTheft.map((item, index) => {
                                    var dataDetail = [
                                        item.transportLineName, 
                                        item.economic,
                                        item.operatorName,
                                        item.operatorPhone,
                                        item.address
                                    ];
                                    return {
                                        listItemIdentifier: item.shipmentNumber,
                                        ordinalNumber: index + 1,
                                        textContent: {
                                            parameterTitle: {
                                                type: 'PlainText',
                                                text: item.shipmentNumber
                                            },
                                            parameterSubtitle: {
                                                type: 'RichText',
                                                text: dataDetail.filter(item => item.length > 0).map(item => `• ${item}`).join('<br>')
                                            },
                                            total: {
                                                type: 'PlainText',
                                                text: ''
                                            }
                                        },
                                        token: item.shipmentNumber
                                    };
                                })
                            }
                        },
                        token: handlerInput.t(`SERVICES_IN_THE_PROCESS_OF_THEFT_MSG`)
                    });
                }
                
                if (data.length > 0) {
                    logic.getMultiVerticalGridDetailsScreen(handlerInput, data);
                } else {
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const DetailsAnomaliesByTransportLineIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'DetailsAnomaliesByTransportLineIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (parseInt(model.totalAnomalies) > 0) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        DetailsAnomaliesByTransportLineIntent_DATA_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_TOTAL_MSG`, {
                            count: parseInt(model.totalAnomalies)
                        }), 
                        DetailsAnomaliesByTransportLineIntent_DATA_CONJUNCTION_RESULT: (function () {
                            if (model.bestTransportLineName === model.worstTransportLineName) {
                                return handlerInput.t(`${intent.name}_DATA_UNIQUE_TRANSPORTLINE_MSG`, {
                                    bestTransportLineName: model.bestTransportLineName,
                                    percentageAnomalyOfBestTransportLine: model.percentageAnomalyOfBestTransportLine
                                });
                            } else {
                                return handlerInput.t(`${intent.name}_DATA_CONJUNCTION_MSG`, {
                                    bestTransportLineName: model.bestTransportLineName,
                                    percentageAnomalyOfBestTransportLine: model.percentageAnomalyOfBestTransportLine,
                                    worstTransportLineName: model.worstTransportLineName,
                                    percentageAnomalyOfWorstTransportLine: model.percentageAnomalyOfWorstTransportLine
                                });
                            }
                        })()
                    });
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                if (model.anomalies.length > 0) {
                    var items = model.anomalies.map((item, index) => {
                        return {
                            listItemIdentifier: item.anomalyName,
                            ordinalNumber: index + 1,
                            textContent: {
                                parameterTitle: {
                                    type: 'PlainText',
                                    text: item.anomalyName
                                },
                                parameterSubtitle: {
                                    type: 'RichText',
                                    text: ''
                                },
                                total: {
                                    type: 'PlainText',
                                    text: item.total
                                }
                            },
                            token: item.anomalyName
                        };
                    });
                    logic.getVerticalGridScreen(handlerInput, items);
                } else {
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const AnomalyStatusByTransportLineIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'AnomalyStatusByTransportLineIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (model.transportLineName !== null) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        transportLineName: model.transportLineName,
                        percentageTransportLine: model.percentageTransportLine,
                        anomalyName: model.anomalyName,
                        operatorName: model.operatorName
                    });
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                if (model.anomalies.length > 0) {
                    var items = model.anomalies.map((item, index) => {
                        return {
                            listItemIdentifier: item.anomalyName,
                            ordinalNumber: index + 1,
                            textContent: {
                                parameterTitle: {
                                    type: 'PlainText',
                                    text: item.anomalyName
                                },
                                parameterSubtitle: {
                                    type: 'RichText',
                                    text: ''
                                },
                                total: {
                                    type: 'PlainText',
                                    text: item.total
                                }
                            },
                            token: item.anomalyName
                        };
                    });
                    logic.getVerticalGridScreen(handlerInput, items);
                } else {
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const StatusAnolamiesByOperatorIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'StatusAnolamiesByOperatorIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (model.bestOperatorName !== null) {
                    if (model.bestOperatorName !== model.worstOperatorName) {
                        voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                            bestOperatorName: model.bestOperatorName,
                            percentageBestOperator: model.percentageBestOperator,
                            worstOperatorName: model.worstOperatorName,
                            percentageWorstOperator: model.percentageWorstOperator
                        });
                    } else {
                        voiceData = handlerInput.t(`${intent.name}_DATA_UNIQUE_MSG`, {
                            bestOperatorName: model.bestOperatorName,
                            percentageBestOperator: model.percentageBestOperator
                        });
                    }
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                if (model.anomalies.length > 0) {
                    var items = model.anomalies.map((item, index) => {
                        return {
                            listItemIdentifier: item.anomalyName,
                            ordinalNumber: index + 1,
                            textContent: {
                                parameterTitle: {
                                    type: 'PlainText',
                                    text: item.anomalyName
                                },
                                parameterSubtitle: {
                                    type: 'RichText',
                                    text: ''
                                },
                                total: {
                                    type: 'PlainText',
                                    text: item.total
                                }
                            },
                            token: item.anomalyName
                        };
                    });
                    logic.getVerticalGridScreen(handlerInput, items);
                } else {
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const StatusAnomaliesByShipmentNumberIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'StatusAnomaliesByShipmentNumberIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {
            slotValue = intent.slots.shipmentNumber.value;           

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (model.shipmentNumber !== null) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        shipmentNumber: model.shipmentNumber,
                        StatusAnomaliesByShipmentNumberIntent_DATA_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_TOTAL_MSG`, {
                            count: parseInt(model.totalAnomalies)
                        })
                    });
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                if (model.anomalies.length > 0) {
                    var items = model.anomalies.map((item, index) => {
                        return {
                            listItemIdentifier: item.anomalyName,
                            ordinalNumber: index + 1,
                            textContent: {
                                parameterTitle: {
                                    type: 'PlainText',
                                    text: item.anomalyName
                                },
                                parameterSubtitle: {
                                    type: 'RichText',
                                    text: ''
                                },
                                total: {
                                    type: 'PlainText',
                                    text: item.total
                                }
                            },
                            token: item.anomalyName
                        };
                    });
                    logic.getVerticalGridScreen(handlerInput, items);
                } else {
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const LastTrimesterAnomalyStatusIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'LastTrimesterAnomalyStatusIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (parseInt(model.totalServices) > 0) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        percentageServices: model.percentageServices,
                        LastTrimesterAnomalyStatusIntent_DATA_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_TOTAL_MSG`, {
                            count: parseInt(model.totalServices)
                        })
                    });
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                
                var data = [];
                
                if (model.worstTransportLines.length > 0 || model.worstOperators.length > 0) {
                    if (model.worstTransportLines.length > 0)  {
                        data.push({
                            listItemIdentifier: handlerInput.t(`MOST_ANOMALOUS_TRANSPORT_LINES_MSG`),
                            textContent: {
                                secondaryText: {
                                    type: "PlainText",
                                    text: handlerInput.t(`MOST_ANOMALOUS_TRANSPORT_LINES_MSG`)
                                }
                            },
                            listTemplate1ListData: {
                                type: "list",
                                listId: "lt1Sample",
                                totalNumberOfItems: 10,
                                listPage: {
                                    listItems: model.worstTransportLines.map((item, index) => {
                                        return {
                                            listItemIdentifier: item.transportLineName,
                                            ordinalNumber: index + 1,
                                            textContent: {
                                                parameterTitle: {
                                                    type: 'PlainText',
                                                    text: item.transportLineName
                                                },
                                                parameterSubtitle: {
                                                    type: 'RichText',
                                                    text: item.contactName
                                                },
                                                total: {
                                                    type: 'PlainText',
                                                    text: item.total
                                                }
                                            },
                                            token: item.transportLineName
                                        };
                                    })
                                }
                            },
                            token: handlerInput.t(`MOST_ANOMALOUS_TRANSPORT_LINES_MSG`)
                        });
                    }
                    
                    if (model.worstOperators.length > 0)  {
                        data.push({
                            listItemIdentifier: handlerInput.t(`MOST_ANOMALOUS_OPERATORS_MSG`),
                            textContent: {
                                secondaryText: {
                                    type: "PlainText",
                                    text: handlerInput.t(`MOST_ANOMALOUS_OPERATORS_MSG`)
                                }
                            },
                            listTemplate1ListData: {
                                type: "list",
                                listId: "lt1Sample",
                                totalNumberOfItems: 10,
                                listPage: {
                                    listItems: model.worstOperators.map((item, index) => {
                                        return {
                                            listItemIdentifier: item.operatorName,
                                            ordinalNumber: index + 1,
                                            textContent: {
                                                parameterTitle: {
                                                    type: 'PlainText',
                                                    text: item.operatorName
                                                },
                                                parameterSubtitle: {
                                                    type: 'RichText',
                                                    text: item.operatorPhone
                                                },
                                                total: {
                                                    type: 'PlainText',
                                                    text: item.total
                                                }
                                            },
                                            token: item.operatorName
                                        };
                                    })
                                }
                            },
                            token: handlerInput.t(`MOST_ANOMALOUS_OPERATORS_MSG`)
                        });
                    }
                }
                
                if (data.length > 0) {
                    logic.getMultiVerticalGridScreen(handlerInput, data);
                } else {
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const TheftStatusLastQuarterIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'TheftStatusLastQuarterIntent';
    },
    async handle(handlerInput) {
        const {attributesManager, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const deviceId = requestEnvelope.context.System.device.deviceId;
        const {intent} = requestEnvelope.request;

        let voiceData = '';
        let slotValue = '0';

        if (!sessionAttributes['name'] || !sessionAttributes['email']) {
            var resultPermission = await logic.validPermissions(handlerInput);
            if (resultPermission.success) {
                return handlerInput.responseBuilder.speak(resultPermission.message).withAskForPermissionsConsentCard(resultPermission.permissions).getResponse();
            } else {
                return handlerInput.responseBuilder.speak(resultPermission.message).getResponse();
            }
        }

        if (!sessionAttributes['deviceId']) {
            var resultRegister = logic.validRegister(handlerInput);
            if (!resultRegister.success) {
                return handlerInput.responseBuilder.speak(resultRegister.message).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            }            
        }

        try {

            try { // call the progressive response service
                await logic.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG'));
            } catch (error) { // if it fails we can continue, but the user will wait without progressive response
                console.log("Progressive directive error : " + error);
            }

            let response = await logic.httpPost(intent.name, {"deviceId": logic.encodeBase64(deviceId), "slotValue": logic.encodeBase64(slotValue)});
            if (response.success) {
                const model = response.items;
                if (parseInt(model.totalServicesValidated) > 0) {
                    voiceData = handlerInput.t(`${intent.name}_DATA_MSG`, {
                        TheftStatusLastQuarterIntent_DATA_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_TOTAL_MSG`, {
                            count: parseInt(model.totalServicesValidated)
                        }),
                        totalPercentageServicesValidated: parseInt(model.totalPercentageServicesValidated),
                        TheftStatusLastQuarterIntent_DATA_PHYSICAL_VALIDATIONS_RESULT: (function () {
                            if (parseInt(model.totalServicesPhysicalValidated) > 0) {
                                return handlerInput.t(`${intent.name}_DATA_PHYSICAL_VALIDATIONS_MSG`, {
                                    TheftStatusLastQuarterIntent_DATA_PHYSICAL_VALIDATIONS_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_PHYSICAL_VALIDATIONS_TOTAL_MSG`, {
                                        count: parseInt(model.totalServicesPhysicalValidated)
                                    })
                                });
                            } else {
                                return handlerInput.t(`${intent.name}_DATA_NO_PHYSICAL_VALIDATIONS_MSG`);
                            }
                        })(),
                        TheftStatusLastQuarterIntent_DATA_THEFT_RESULT: (function () {
                            if (parseInt(model.totalServicesTheft) > 0) {
                                return handlerInput.t(`${intent.name}_DATA_THEFT_MSG`, {
                                    TheftStatusLastQuarterIntent_DATA_THEFT_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_TOTAL_MSG`, {
                                        count: parseInt(model.totalServicesTheft)
                                    }),
                                    TheftStatusLastQuarterIntent_DATA_THEFT_CONJUNCTION_RESULT: (function () {
                                        if (parseInt(model.totalServicesConsummatedTheft) > 0 && parseInt(model.totalServicesTheftRecovered) > 0) {
                                            return handlerInput.t(`${intent.name}_DATA_THEFT_CONJUNCTION_MSG`, {
                                                TheftStatusLastQuarterIntent_DATA_THEFT_CONSSUMATED_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_CONSSUMATED_MSG`, {
                                                    TheftStatusLastQuarterIntent_DATA_THEFT_CONSSUMATED_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_CONSSUMATED_TOTAL_MSG`, {
                                                        count: parseInt(model.totalServicesConsummatedTheft)
                                                    }),
                                                    totalPercentageConsummatedTheftAndSinister: parseInt(model.totalPercentageConsummatedTheftAndSinister)
                                                }),
                                                TheftStatusLastQuarterIntent_DATA_THEFT_RECOVERED_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_RECOVERED_MSG`, {
                                                    TheftStatusLastQuarterIntent_DATA_THEFT_RECOVERED_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_RECOVERED_TOTAL_MSG`, {
                                                        count: parseInt(model.totalServicesTheftRecovered)
                                                    }),
                                                    totalPercentageServicesTheftRecovered: parseInt(model.totalPercentageServicesTheftRecovered)
                                                })
                                            });
                                        } else if (parseInt(model.totalServicesConsummatedTheft) > 0 && parseInt(model.totalServicesTheftRecovered) === 0) {
                                            return handlerInput.t(`${intent.name}_DATA_THEFT_CONSSUMATED_MSG`, {
                                                TheftStatusLastQuarterIntent_DATA_THEFT_CONSSUMATED_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_CONSSUMATED_TOTAL_MSG`, {
                                                    count: parseInt(model.totalServicesConsummatedTheft)
                                                }),
                                                totalPercentageConsummatedTheftAndSinister: parseInt(model.totalPercentageConsummatedTheftAndSinister)
                                            });
                                        } else if (parseInt(model.totalServicesConsummatedTheft) === 0 && parseInt(model.totalServicesTheftRecovered) > 0) {
                                            return handlerInput.t(`${intent.name}_DATA_THEFT_RECOVERED_MSG`, {
                                                TheftStatusLastQuarterIntent_DATA_THEFT_RECOVERED_TOTAL_RESULT: handlerInput.t(`${intent.name}_DATA_THEFT_RECOVERED_TOTAL_MSG`, {
                                                    count: parseInt(model.totalServicesTheftRecovered)
                                                }),
                                                totalPercentageServicesTheftRecovered: parseInt(model.totalPercentageServicesTheftRecovered)
                                            });
                                        } else {
                                            return '';
                                        }
                                    })()
                                });
                            } else {
                                return handlerInput.t(`${intent.name}_DATA_NO_THEFT_MSG`);
                            }
                        })()
                    });
                } else {
                    voiceData = handlerInput.t(`${intent.name}_NO_DATA_MSG`);
                }
                
                var data = [];
                
                if (model.transportLinesMostThefts.length > 0 || model.destiniesMostThefts.length > 0) {
                    if (model.transportLinesMostThefts.length > 0)  {
                        data.push({
                            listItemIdentifier: handlerInput.t(`TRANSPORT_LINES_WITH_THE_MOST_THEFTS_MSG`),
                            textContent: {
                                secondaryText: {
                                    type: "PlainText",
                                    text: handlerInput.t(`TRANSPORT_LINES_WITH_THE_MOST_THEFTS_MSG`)
                                }
                            },
                            listTemplate1ListData: {
                                type: "list",
                                listId: "lt1Sample",
                                totalNumberOfItems: 10,
                                listPage: {
                                    listItems: model.transportLinesMostThefts.map((item, index) => {
                                        return {
                                            listItemIdentifier: item.transportLineName,
                                            ordinalNumber: index + 1,
                                            textContent: {
                                                parameterTitle: {
                                                    type: 'PlainText',
                                                    text: item.transportLineName
                                                },
                                                parameterSubtitle: {
                                                    type: 'RichText',
                                                    text: item.contactName
                                                },
                                                total: {
                                                    type: 'PlainText',
                                                    text: item.total
                                                }
                                            },
                                            token: item.transportLineName
                                        };
                                    })
                                }
                            },
                            token: handlerInput.t(`TRANSPORT_LINES_WITH_THE_MOST_THEFTS_MSG`)
                        });
                    }
                    
                    if (model.destiniesMostThefts.length > 0)  {
                        data.push({
                            listItemIdentifier: handlerInput.t(`DESTINATIONS_WITH_THE_MOST_THEFTS_MSG`),
                            textContent: {
                                secondaryText: {
                                    type: "PlainText",
                                    text: handlerInput.t(`DESTINATIONS_WITH_THE_MOST_THEFTS_MSG`)
                                }
                            },
                            listTemplate1ListData: {
                                type: "list",
                                listId: "lt1Sample",
                                totalNumberOfItems: 10,
                                listPage: {
                                    listItems: model.destiniesMostThefts.map((item, index) => {
                                        return {
                                            listItemIdentifier: item.destinyName,
                                            ordinalNumber: index + 1,
                                            textContent: {
                                                parameterTitle: {
                                                    type: 'PlainText',
                                                    text: item.destinyName
                                                },
                                                parameterSubtitle: {
                                                    type: 'RichText',
                                                    text: item.state
                                                },
                                                total: {
                                                    type: 'PlainText',
                                                    text: item.total
                                                }
                                            },
                                            token: item.destinyName
                                        };
                                    })
                                }
                            },
                            token: handlerInput.t(`DESTINATIONS_WITH_THE_MOST_THEFTS_MSG`)
                        });
                    }
                }
                
                if (data.length > 0) {
                    logic.getMultiVerticalGridScreen(handlerInput, data);
                } else {
                    logic.getSimpleScreen(handlerInput, voiceData);
                }
                
                voiceData += handlerInput.t('SHORT_HELP_MSG');
                return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
            } else {
                voiceData = handlerInput.t(`${response.code}_MSG`);
                logic.getSimpleScreen(handlerInput, voiceData);
            }
            return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        } catch (error) {
            console.log(error);
            return handlerInput.responseBuilder.speak(handlerInput.t('NOT_DATA_FOUND_MSG')).reprompt(handlerInput.t('HELP_MSG')).getResponse();
        }
    }
};
const TouchIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'Alexa.Presentation.APL.UserEvent';
    },
    handle(handlerInput) {
        let touchItem = JSON.parse(handlerInput.requestEnvelope.request.arguments[0]);
        let speechText = `${touchItem.touchSpeech.value}. `; // handlerInput.t('LIST_PERSON_DETAIL_MSG', {name: person.humanLabel.value, dateofbirth: person.date_of_birth.value});

        return handlerInput.responseBuilder.speak(speechText).reprompt(handlerInput.t('HELP_MSG')).getResponse();
    }
};
const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const voiceData = handlerInput.t('HELP_MSG');

        logic.getLaunchScreen(handlerInput, voiceData);

        return handlerInput.responseBuilder.speak(voiceData).reprompt(voiceData).getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent' || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        const name = sessionAttributes['name'] ? sessionAttributes['name'] : '';

        const voiceData = handlerInput.t('GOODBYE_MSG', {name: name});

        return handlerInput.responseBuilder.speak(voiceData).withShouldEndSession(true).getResponse();
    }
};
const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        const voiceData = handlerInput.t('FALLBACK_MSG');

        return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) { // Any cleanup logic goes here.
        return handlerInput.responseBuilder.withShouldEndSession(true).getResponse();
    }
};
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = handlerInput.requestEnvelope.request.intent.name;
        const voiceData = handlerInput.t('REFLECTOR_MSG', {intent: intentName});

        return handlerInput.responseBuilder.speak(voiceData)
        // .reprompt('add a reprompt if you want to keep the session open for the user to respond').getResponse();
    }
};
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        const voiceData = handlerInput.t('ERROR_MSG');

        console.log(`~~~~ Error handled: ${error.message}`);

        return handlerInput.responseBuilder.speak(voiceData).reprompt(handlerInput.t('HELP_MSG')).getResponse();
    }
};
module.exports = {
    LaunchRequestHandler,
    RegisterDeviceIntentHandler,
    //Intents
    StatusCurrentServicesIntentHandler,
    SummaryByTransportLineIntentHandler,
    DetailsByShipmentNumberIntentHandler,
    DepartureStatusLastQuarterIntentHandler,
    CurrentSecurityStatusIntentHandler,
    DetailsAnomaliesByTransportLineIntentHandler,
    AnomalyStatusByTransportLineIntentHandler,
    StatusAnolamiesByOperatorIntentHandler,
    StatusAnomaliesByShipmentNumberIntentHandler,
    LastTrimesterAnomalyStatusIntentHandler,
    TheftStatusLastQuarterIntentHandler,
    //Intents
    TouchIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    FallbackIntentHandler,
    SessionEndedRequestHandler,
    IntentReflectorHandler,
    ErrorHandler
};